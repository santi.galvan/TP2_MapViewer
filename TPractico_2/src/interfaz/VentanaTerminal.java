package interfaz;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class VentanaTerminal extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private JTextField nombre;
	private JTextField nombreProvincia;
	private JTextField habitantes;

	public VentanaTerminal(JFrame frmAgmAplication, boolean modal)
	{
		super(frmAgmAplication, modal);
		setUndecorated(true);
		setTitle("Ingresar datos");
		setResizable(false);
		setBounds(650, 400, 350, 300);
		getContentPane().setLayout(null);
		
		campoNombre();
		campoProvincia();
		campoHabitantes();
		
		carteles();
		botonOk();

		JLabel fondo = new JLabel();
		getContentPane().add(fondo);
		fondo.setBounds(0, -11, 344, 300);
		fondo.setIcon(new ImageIcon(VentanaTerminal.class.getResource("/interfaz/imagenes/datosMenu.png")));
	}

	private void campoNombre() 
	{
		nombre = new JTextField();
		getContentPane().add(nombre);
		nombre.setBorder(null);
		nombre.setBounds(120, 114, 196, 21);
		nombre.setFont(new Font("Dialog", Font.BOLD, 11));
		nombre.setColumns(10);
	}

	private void campoProvincia() 
	{
		nombreProvincia = new JTextField();
		getContentPane().add(nombreProvincia);
		nombreProvincia.setBounds(120, 170, 196, 21);
		nombreProvincia.setBorder(null);
		nombreProvincia.setFont(new Font("Dialog", Font.BOLD, 11));
		nombreProvincia.setColumns(10);
	}

	private void campoHabitantes() 
	{
		habitantes = new JTextField();

		habitantes.setBorder(null);
		habitantes.setFont(new Font("Dialog", Font.BOLD, 11));
		habitantes.setColumns(10);
		habitantes.setBounds(120, 220, 196, 21);
		habitantes.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, habitantes);
			}
		});
		getContentPane().add(habitantes);
	}

	private void carteles() 
	{
		JLabel textoCostoPorKM = new JLabel("Nombre:");
		getContentPane().add(textoCostoPorKM);
		textoCostoPorKM.setBounds(110, 91, 124, 19);
		textoCostoPorKM.setForeground(Color.WHITE);
		textoCostoPorKM.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel textoImpuestoProvincial = new JLabel("Provincia:");
		getContentPane().add(textoImpuestoProvincial);
		textoImpuestoProvincial.setBounds(112, 146, 143, 19);
		textoImpuestoProvincial.setForeground(Color.WHITE);
		textoImpuestoProvincial.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel textoExcesoKM = new JLabel("Cantidad de habitantes:");
		getContentPane().add(textoExcesoKM);
		textoExcesoKM.setBounds(113, 198, 210, 19);
		textoExcesoKM.setForeground(Color.WHITE);
		textoExcesoKM.setFont(new Font("Dialog", Font.BOLD, 14));
	}

	private void botonOk() 
	{
		JButton btnOk = new JButton("Ok");
		getContentPane().add(btnOk);
		btnOk.setBounds(22, 222, 70, 23);
		
		btnOk.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(camposCompletos())
					dispose();
				else
					JOptionPane.showMessageDialog(null, "Por favor, complete todos los campos!");
			}
		});
	}
	
	private void esNumero(KeyEvent e, JTextField campo) 
	{
		char caracter = e.getKeyChar(); 
		if (((caracter < '0') || (caracter > '9')) 
		        && (caracter != KeyEvent.VK_BACK_SPACE)
		        && (caracter != '.' || campo.getText().contains(".")) ) 
		{
		            e.consume();
		            JOptionPane.showMessageDialog(null, "Se admiten numeros");
		}
	}
	
	private boolean camposCompletos()
	{
		return campoOcupado(nombre) && campoOcupado(nombreProvincia) && campoOcupado(habitantes);
	}
	
	private boolean campoOcupado(JTextField campo)
	{
		return campo.getText().length() > 0 ;
	}
	
	public String getNombre()
	{
		return nombre.getText();
	}
	
	public String getNombreProvincia()
	{
		return nombreProvincia.getText();
	}
	
	public Integer getHabitantes()
	{
		if(habitantes.getText().length() > 0)
			return Integer.valueOf(habitantes.getText());
		
		return 0;
	}

	public void limpiarCampos() 
	{
		nombre.setText("");
		nombreProvincia.setText("");
		habitantes.setText("");
	}
	
}