package interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import negocio.Manager;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class MainForm 
{
	private BufferedImage img;
	private JFrame frmAgmAplication;
	private JMapViewer mapa;
	private JLabel ejecutar;
	private JLabel guardar;
	private JLabel cargar;
	private JLabel configCostos;
	private JLabel info;
	private JLabel fondo;
	private VentanaCostos menuPrecios;
	private VentanaTerminal ventanaAuxiliar;
	private VentanaResultado ventanaResultado;
	private JLabel ventanaResultadoSecundario;
	private Manager manager;
	private JTextField resultado;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try{
					MainForm window = new MainForm();
					window.frmAgmAplication.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		manager = new Manager();
		menuPrecios = new VentanaCostos(frmAgmAplication, true);
		ventanaResultado = new VentanaResultado(frmAgmAplication, true);
		ventanaAuxiliar = new VentanaTerminal(frmAgmAplication, true);
		
		frmAgmAplication = new JFrame();
		frmAgmAplication.setResizable(false);
		frmAgmAplication.setTitle("AGM Aplication");
		frmAgmAplication.setBounds(500, 150, 680, 700);
		frmAgmAplication.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgmAplication.getContentPane().setLayout(null);

		mapa();

		//Botones interactivos
		botonEjecutar();
		botonConfigCostos();
		botonInfo();
		botonCargar();
		botonGuardar();
		
		//Detalles est�ticos
		resultadoPasivo();
		fondo();
		iconoPosteElectrico();
	}
	
	/**METODOS DE DIBUJADO DE PANTALLA**/
	private void fondo() 
	{
		fondo = new JLabel();
		fondo.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/fondotp2.png")));
		fondo.setBounds(0, 0, 664, 661);
		frmAgmAplication.getContentPane().add(fondo);
	}

	private void mapa() 
	{
		mapa = new JMapViewer();
		mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(e.getButton() == MouseEvent.BUTTON1) 
				{
					ventanaAuxiliar.setVisible(true);
					
					if(!manager.checkearEntrada(ventanaAuxiliar.getNombre(), ventanaAuxiliar.getNombreProvincia(), ventanaAuxiliar.getHabitantes(), mapa.getPosition(e.getPoint()).getLat(), mapa.getPosition(e.getPoint()).getLon())) 
					{
					
						manager.cargarDato(ventanaAuxiliar.getNombre(), ventanaAuxiliar.getNombreProvincia(), ventanaAuxiliar.getHabitantes(), mapa.getPosition(e.getPoint()).getLat(), mapa.getPosition(e.getPoint()).getLon());
						
						
						MapMarker marker = new MapMarkerDot("  "+ventanaAuxiliar.getNombre(), mapa.getPosition(e.getPoint())); //Dejo espacio para que no se pise con la imagen
						mapa.addMapMarker(marker);
						mapa.addMapMarker(new IconMarker(mapa.getPosition(e.getPoint()), img)); 
						
						ventanaAuxiliar.limpiarCampos();
					}
					else{
						JOptionPane.showMessageDialog(null, "Terminal ya existente!");
					}
				}
			}
		});
		mapa.setBounds(22, 168, 620, 458);
		mapa.setZoomContolsVisible(false);
		mapa.setDisplayPositionByLatLon(-34.521, -58.7008, 11);
		frmAgmAplication.getContentPane().add(mapa);
	}

	//BOTONES
	private void botonGuardar() {
		guardar = new JLabel("");
		guardar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if(e.getButton() == MouseEvent.BUTTON1)
					guardarArchivo();
			}
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				guardar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonSaveOn.png")));
				guardar.setToolTipText("Guardar");
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				guardar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonSaveOff.png")));
			}
		});
		guardar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonSaveOff.png")));
		guardar.setBounds((frmAgmAplication.getWidth()/2 + 180), 50, 95, 100);
		frmAgmAplication.getContentPane().add(guardar);
	}

	private void guardarArchivo() 
	{
		JFileChooser fc = new JFileChooser();
		int seleccion = fc.showSaveDialog(frmAgmAplication.getContentPane());
		
		if(seleccion == JFileChooser.APPROVE_OPTION)
		{
		    File fichero = fc.getSelectedFile();
		    try
		    {
		    	manager.guardarSolucion(fichero.getAbsolutePath()+".map");
		    } catch (Exception ex) 
		    {
		        ex.printStackTrace();
		    }
		}
	}
	
	private void botonCargar() 
	{
		cargar = new JLabel();
		cargar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(e.getButton() == MouseEvent.BUTTON1) 
					cargarArchivo();
			}
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				cargar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonOpenOn.png")));
				cargar.setToolTipText("Cargar");
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				cargar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonOpenOff.png")));
			}
		});
		cargar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonOpenOff.png")));
		cargar.setBounds(frmAgmAplication.getWidth()/2 + 65, 50, 95, 100);
		frmAgmAplication.getContentPane().add(cargar);
	}

	private void cargarArchivo() 
	{
		JFileChooser fc=new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.MAP", "map");
		fc.setFileFilter(filtro);
		 
		int seleccion = fc.showOpenDialog(frmAgmAplication.getContentPane());
		if(seleccion == JFileChooser.APPROVE_OPTION)
		{
		    File fichero = fc.getSelectedFile();
		    try{
		    	manager.cargarArchivo(fichero.getAbsolutePath());
		    	dibujarRedElectrica();
		    	mapa.repaint();
		    }
		    catch (Exception ex) {
		    	ex.printStackTrace();
		    }
		}
	}

	private void botonInfo() 
	{
		info = new JLabel();
		info.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				info.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonInfoOn.png")));
				info.setToolTipText("Info");
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				info.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonInfoOff.png")));
			}
		});
		info.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonInfoOff.png")));
		info.setBounds(frmAgmAplication.getWidth()/2 - 165, 50, 95, 100);
		frmAgmAplication.getContentPane().add(info);
	}

	private void botonConfigCostos()
	{
		configCostos = new JLabel();
		configCostos.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				configCostos.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonConfigCostosOn.png")));
				configCostos.setToolTipText("Costos");
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				configCostos.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonConfigCostosOff.png")));
			}
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				panelConfiguraciones();
			
			}
		});
		configCostos.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonConfigCostosOff.png")));
		configCostos.setBounds(frmAgmAplication.getWidth()/2 - 280, 50, 95, 100);
		frmAgmAplication.getContentPane().add(configCostos);
	}

	private void panelConfiguraciones() 
	{
		menuPrecios.setearCampos(manager.costosAnteriores());
		menuPrecios.setVisible(true);
		manager.cargarCosto(menuPrecios.getCableado(),menuPrecios.getImpuesto(), menuPrecios.getKilometros());
	}
	
	private void botonEjecutar()
	{
		ejecutar = new JLabel();
		ejecutar.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(e.getButton() == MouseEvent.BUTTON1) 
				{
					frmAgmAplication.repaint();
					manager.ejecutar();
					dibujarRedElectrica();
					ventanaSecundaria();
				}
			}
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				ejecutar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonMapOn.png")));
				ejecutar.setToolTipText("Ejecutar");
			}
			@Override
			public void mouseExited(MouseEvent e)
			{
				ejecutar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonMapOff.png")));
			}
		});
		ejecutar.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/botonMapOff.png")));
		ejecutar.setBounds(frmAgmAplication.getWidth()/2 - 50, 50, 95, 100);
		frmAgmAplication.getContentPane().add(ejecutar);
	}
	
	private void ventanaSecundaria() {
		ventanaResultado.setCosto(manager.costoTotal());
		DecimalFormat df = new DecimalFormat("#.000");
		resultado.setText("$ "+String.valueOf(df.format(manager.costoTotal())));
		ventanaResultado.setVisible(true);		
		ventanaResultadoSecundario.setVisible(true);
		resultado.setVisible(true);
	}	
	
	private void resultadoPasivo() {
		ventanaResultadoSecundario = new JLabel("");
		ventanaResultadoSecundario.setIcon(new ImageIcon(MainForm.class.getResource("/interfaz/imagenes/mostrarcosto.png")));
		ventanaResultadoSecundario.setBounds(0, 0, 250, 70);
		ventanaResultadoSecundario.setVisible(false);
		
		resultado = new JTextField();
		resultado.setHorizontalAlignment(SwingConstants.CENTER);
		resultado.setEditable(false);
		resultado.setEnabled(false);
		resultado.setBorder(null);
		resultado.setOpaque(false);
		resultado.setVisible(false);
		resultado.setForeground(new Color(50, 205, 50));
		resultado.setFont(new Font("Dialog", Font.BOLD, 20));
		resultado.setBounds(149, 30, 86, 20);
		mapa.add(resultado);
		resultado.setColumns(10);
		mapa.add(ventanaResultadoSecundario);
	}
	
	private void dibujarRedElectrica() 
	{
		HashMap<Coordinate, Coordinate> coordenadas = manager.coordeandas();
		for(Coordinate coord : coordenadas.keySet()) 
		{
			dibujarCoordenada("", coord);
			dibujarCoordenada("", coordenadas.get(coord));
			dibujarAristaSolucion(coord, coordenadas.get(coord));
		}
	}
	
	public void dibujarCoordenada(String nombre, Coordinate coordenada) 
	{
		MapMarker marker = new MapMarkerDot("   "+nombre, coordenada); //Dejo espacio para que no se pise con la imagen
		mapa.addMapMarker(marker);
		mapa.addMapMarker(new IconMarker(coordenada, img)); 
	}

	private void dibujarAristaSolucion(Coordinate origen, Coordinate destino)
	{
		ArrayList<Coordinate> coordenadas = new ArrayList<>();
		coordenadas.add(origen);
		coordenadas.add(destino);
		coordenadas.add(origen);
		
		MapPolygon polygon = new MapPolygonImpl("", coordenadas);
		polygon.getStyle().setColor(Color.GREEN);
		mapa.addMapPolygon(polygon);
		mapa.repaint();
	}
	
	private void iconoPosteElectrico() 
	{
		img = null;
		try {
		    img = ImageIO.read(MainForm.class.getResource("/interfaz/imagenes/poste.png"));
		} catch (IOException e) {
			throw new RuntimeException("No se pudo cargar el icono del poste");
		}
	}
	
	/**METODOS DE ENCUADRADO*/
	@SuppressWarnings("unused")
	private void reglasVerticales() {
		int altoVentana = frmAgmAplication.getWidth();
		
		JSeparator mitadVertical = new JSeparator();
		mitadVertical.setBackground(Color.green);
		mitadVertical.setOrientation(SwingConstants.VERTICAL);
		mitadVertical.setBounds(altoVentana/2 - 10, 0, 2, 1000);
		frmAgmAplication.getContentPane().add(mitadVertical);	
		
		
		JSeparator primeraMitadVertical = new JSeparator();
		primeraMitadVertical.setBackground(Color.green);
		primeraMitadVertical.setOrientation(SwingConstants.VERTICAL);
		primeraMitadVertical.setBounds(altoVentana/4 - 10, 0, 2, 1000);
		frmAgmAplication.getContentPane().add(primeraMitadVertical);	

		JSeparator segundaMitadVertical = new JSeparator();
		segundaMitadVertical.setBackground(Color.green);
		segundaMitadVertical.setOrientation(SwingConstants.VERTICAL);
		segundaMitadVertical.setBounds(altoVentana/2 + altoVentana/4 - 10, 0, 2, 1000);
		frmAgmAplication.getContentPane().add(segundaMitadVertical);
	}

	@SuppressWarnings("unused")
	private void reglasHorizontales() {
		int anchoVentana = frmAgmAplication.getHeight();
		
		JSeparator mitadHorizontal = new JSeparator();
		mitadHorizontal.setBackground(Color.red);
		mitadHorizontal.setBounds(0, anchoVentana/2 - 20, 1000, 2);
		frmAgmAplication.getContentPane().add(mitadHorizontal);	
		
		JSeparator primeraMitadHorizontal = new JSeparator();
		primeraMitadHorizontal.setBackground(Color.red);
		primeraMitadHorizontal.setBounds(0, (anchoVentana/2- 20) /2, 1000, 2);
		frmAgmAplication.getContentPane().add(primeraMitadHorizontal);	
				
		JSeparator segundaMitadHorizontal = new JSeparator();
		segundaMitadHorizontal.setBackground(Color.red);
		segundaMitadHorizontal.setBounds(0, (anchoVentana/2) + anchoVentana/4 - 20, 1000, 2);
		frmAgmAplication.getContentPane().add(segundaMitadHorizontal);
	}
}

