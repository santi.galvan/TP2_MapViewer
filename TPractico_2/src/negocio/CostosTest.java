package negocio;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.junit.Test;

public class CostosTest {

	@Test(expected = IllegalArgumentException.class)
	public void LocalidadNullTest() 
	{
		Localidad localidad = new Localidad("San Miguel", "Buenos Aires", 747, -34.541913994732255, -58.717288970947266);
		
		Costos costo = new Costos();
		
		costo.calcularCosto(localidad, null);
		costo.calcularCosto(null, localidad);
		costo.calcularCosto(null, null);
	}
	
	@Test
	public void MismaProvinciaTest()
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Fonavi, Buenos Aires, 346, -34.44560067000515, -58.73711585998535"});
		
		Costos costos = new Costos(1,20,20);
		
		Conexion conexion = new Conexion(localidades.get(0), localidades.get(1),costos);
		BigDecimal instancia = conexion.getPeso();
		
		BigDecimal esperado = new BigDecimal(costos.calcularKm(localidades.get(0), localidades.get(1)));
		esperado = esperado.setScale(4, RoundingMode.CEILING);

		assertEquals(esperado, instancia);
	}
	
	@Test
	public void DistintaProvinciaTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Fonavi, Chaco, 346, -34.44560067000515, -58.73711585998535"});
		
		Costos costos = new Costos(1,20,20);
		
		Conexion conexion = new Conexion(localidades.get(0), localidades.get(1),costos);
		BigDecimal instancia = conexion.getPeso();
		
		BigDecimal esperado = new BigDecimal(costos.calcularKm(localidades.get(0), localidades.get(1))+20);
		esperado = esperado.setScale(4, RoundingMode.CEILING);

		assertEquals(esperado, instancia);
	}

	@Test
	public void DistanciaMayorA200Test()
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Fonavi, Chaco, 346, -34.44560067000515, -58.73711585998535"});
		
		Costos costos = new Costos(1,20,20);
		
		Conexion conexion = new Conexion(localidades.get(0), localidades.get(1),costos);
		BigDecimal instancia = conexion.getPeso();
		
		BigDecimal esperado = new BigDecimal(costos.calcularKm(localidades.get(0), localidades.get(1))+20);
		esperado = esperado.setScale(4, RoundingMode.CEILING);

		assertEquals(esperado, instancia);
	}

	@Test
	public void DistintaMayorA200YMismaProvinciaTest() 
	{	
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Olavarria, Buenos Aires, 346, -37.71859032558814, -40.078125"});
		
		Costos costos = new Costos(1,20,20);
		
		Conexion conexion = new Conexion(localidades.get(0), localidades.get(1),costos);
		BigDecimal instancia = conexion.getPeso();
		
		BigDecimal esperado = new BigDecimal(costos.calcularKm(localidades.get(0), localidades.get(1))+20);
		esperado = esperado.setScale(4, RoundingMode.CEILING);

		assertEquals(esperado, instancia);
	}
	
	@Test
	public void DistintaMayorA200YDistintaProvinciaTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Usuahia, Tierra del fuego, 346, -37.71859032558814, -40.078125"});
		
		Costos costos = new Costos(1,20,20);
		
		Conexion conexion = new Conexion(localidades.get(0), localidades.get(1),costos);
		BigDecimal instancia = conexion.getPeso();
		
		BigDecimal esperado = new BigDecimal(costos.calcularKm(localidades.get(0), localidades.get(1))+20+20);
		esperado = esperado.setScale(4, RoundingMode.CEILING);

		assertEquals(esperado, instancia);
	}
	
}
