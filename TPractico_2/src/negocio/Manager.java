package negocio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.openstreetmap.gui.jmapviewer.Coordinate;

//Clase nexo entre Negocio y la Interfaz
public class Manager 
{
	private Negocio logica;
//	corrobora entrada
//	pasa datos a negocio
//	recibe datos de negocio y los envia a mainform
	
	public Manager()
	{
		logica = new Negocio();
	}

	//Entrada
	public void cargarDato(String nombre, String provincia, int habitantes, double latitud, double longitud)
	{
		logica.cargarLocalidad(nombre, provincia, habitantes, latitud, longitud);
	}
	
	public void ejecutar()
	{
		logica.constuirRedOptima();
	}
	
	public void enviarDatosCostos(Integer costoPorKm, Integer impuestoProvincial, Integer excesoDeKm) 
	{
		logica.cargarCostos(costoPorKm, impuestoProvincial, excesoDeKm);
	}
	
	public void cargarCosto(double valorPorKm, double impuestoProvincial, double excesoDeKm)
	{
		logica.cargarCostos(valorPorKm, impuestoProvincial, excesoDeKm);
	}
	
	public boolean checkearEntrada(String nombre, String provincia, int habitantes, double lat, double lon)
	{
		return logica.existeLocalidad(nombre, provincia, habitantes, lat, lon);
	}
	
	public void guardarSolucion(String absolutePath) 
	{
		try 
		{
			FileOutputStream fos = new FileOutputStream(absolutePath);
			ObjectOutputStream out = new ObjectOutputStream(fos);
	    	out.writeObject(logica.getRedElectrica());
	    	out.close();
		}
    	catch (Exception ex) 
		{
    		ex.printStackTrace();
    	}
	}
	
	//Salida
	public HashMap<Coordinate, Coordinate> coordeandas()
	{
		ArrayList<Conexion>  futurasCoordenadas = logica.solucion();
		HashMap<Coordinate, Coordinate> ret = new HashMap<>();
		for(Conexion e : futurasCoordenadas)
		{
			Coordinate c1 = new Coordinate(e.getOrigen().getLatitud(), e.getOrigen().getLongitud());
			Coordinate c2 = new Coordinate(e.getDestino().getLatitud(), e.getDestino().getLongitud());
			ret.put(c1, c2);
		}
		return ret;
	}
	
	public HashMap<Coordinate, Coordinate> coordeandasCompleta()
	{
		ArrayList<Conexion>  futurasCoordenadas = logica.generarConexiones();
		HashMap<Coordinate, Coordinate> ret = new HashMap<>();
		for(Conexion e : futurasCoordenadas)
		{
			Coordinate c1 = new Coordinate(e.getOrigen().getLatitud(), e.getOrigen().getLongitud());
			Coordinate c2 = new Coordinate(e.getDestino().getLatitud(), e.getDestino().getLongitud());
			ret.put(c1, c2);
		}
		return ret;
	}
	
	public double costoTotal()
	{
		return logica.costoTotal();
	}
	
	public boolean existeConexion(String origen, String destino)
	{
		return true;
	}
	
	public double costoDeConexion(String origen, String destino)
	{
		return 4;
	}
	
	public double [] costosAnteriores(){
		return logica.getCostos();
	}
	
	
	//Lectura
	private RedElectrica leerArchivo(String absolutePath) 
	{
		RedElectrica solucion = null;
		try 
		{
			FileInputStream fos = new FileInputStream(absolutePath);
			ObjectInputStream in = new ObjectInputStream(fos);
	    	
			solucion = (RedElectrica) in.readObject();
	    	in.close();
		}
    	catch (Exception ex) 
		{
    		ex.printStackTrace();
    	}
    	
		return solucion;
	}
	
	public void cargarArchivo(String absolutePath)
	{
		RedElectrica solucion = leerArchivo(absolutePath);
		logica.setRedElectrica(solucion);
	}
}
