package negocio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Conexion implements Comparable<Conexion>, Serializable
{
	private Localidad origen; 
	private Localidad destino; //Arista U-V es lo mismo que V-U
	private BigDecimal peso;
	
	private static final long serialVersionUID = 1L;
	
	public Conexion(Localidad origen, Localidad destino, Costos costos) 
	{
		this.origen = origen;
		this.destino = destino;
		this.peso = costos.calcularCosto(origen, destino);
	}	
	
	//Genero todas las aristas posibles entre n vertices
	static public ArrayList<Conexion> conexionesPosibles(ArrayList<Localidad> localidades, Costos costo)
	{
		ArrayList<Conexion> ret = new ArrayList<>();
		
		for(Localidad origen: localidades) 
		{
			for(Localidad destino: localidades)
			{
				Conexion candidata = new Conexion(origen, destino, costo);
				if(!origen.equals(destino) && !ret.contains(candidata)) {
					ret.add(candidata);
				}
			}	
		}
		return ret;
	}
	
	public BigDecimal getPeso(){
		return peso;
	}
	
	public Localidad getDestino()
	{
		return destino;
	}

	public Localidad getOrigen() 
	{
		return origen;
	}
	
	@Override
	public String toString() 
	{
		return "[" + origen + " -> " + destino + ", " + peso + "]";	
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || obj.getClass() != this.getClass()) return false;
		if(this == obj) return true;
		Conexion otra = (Conexion) obj;
		if(otra.hashCode()!=this.hashCode()) return false;
		if((igualAigual(otra) || igualCruzado(otra)) && this.peso.equals(otra.peso)) return true;
		return false;
	}
	
	private boolean igualAigual(Conexion ar)
	{
		return origen.equals(ar.origen) && destino.equals(ar.destino);
	}
	
	private boolean igualCruzado(Conexion ar)
	{
		return origen.equals(ar.destino) && destino.equals(ar.origen);
	}
	
	@Override 
	public int hashCode() //No conocemos muy bien su eficacia. 
	{
		return origen.hashCode() * destino.hashCode() + peso.intValue();
	}
	
	@Override
	public int compareTo(Conexion a)
	{	 
        int estado = -1;
        
        if(peso.compareTo(a.getPeso()) == 0)
            estado = 0;  
        
        else if(peso.compareTo(a.getPeso()) > 0)
            estado = 1; 
        
        return estado;
	}

}
