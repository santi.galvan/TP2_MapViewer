package negocio;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

public class ConexionTest 
{
	@Test
	public void happyPathTest() {
		
		Costos costos = new Costos(100, 100, 100);
		
		Localidad L1 = new Localidad("San Miguel", "Buenos Aires", 1000, 10.44, 24.55);
		Localidad L2 = new Localidad("Grand Bourg", "Buenos Aires", 700, 8.44, 20.23);
		
		Conexion aristaA = new Conexion(L1, L2, costos);
		Conexion aristaB = new Conexion(L2, L1, costos);
		
		assertTrue(aristaA.equals(aristaB));
		assertTrue(aristaB.equals(aristaA));
	}
	
	@Test
	public void probarHashCodeTest(){
		Costos costos = new Costos(100, 100, 100);
		
		Localidad L1 = new Localidad("San Miguel", "Buenos Aires", 1000, 10.44, 24.55);
		Localidad L2 = new Localidad("San Martin", "Buenos Aires", 700, 8.44, 20.23);
		
		Conexion aristaA = new Conexion(L1, L2, costos);
		Conexion aristaB = new Conexion(L2, L1, costos);
		
		assertEquals(aristaA.hashCode(), aristaB.hashCode());
		assertEquals(aristaB.hashCode(), aristaA.hashCode());
		}
	
	@Test
	public void conjuntoAristaTest(){
		Costos costos = new Costos(100, 100, 100);
		Localidad L1 = new Localidad("San Miguel", "Buenos Aires", 1000, 10.44, 24.55);
		Localidad L2 = new Localidad("Grand Bourg", "Buenos Aires", 700, 8.44, 20.23);
		
		Conexion aristaA = new Conexion(L1, L2, costos);
		Conexion aristaB = new Conexion(L2, L1, costos);
		
		HashSet<Conexion> aristas = new HashSet<>();
		aristas.add(aristaA);
		aristas.add(aristaB);
		
		assertEquals(1, aristas.size());
	}
	
	@Test
	public void pesoAristasTest(){
		Costos costos = new Costos(1, 1, 1);
		Localidad L1 = new Localidad("San Miguel", "Buenos Aires", 1000, 10.44, 24.55);
		Localidad L2 = new Localidad("Grand Bourg", "Buenos Aires", 700, 8.44, 20.23);
		
		Conexion aristaA = new Conexion(L1, L2, costos);
		Conexion aristaB = new Conexion(L2, L1, costos);
		
		assertEquals(0, aristaA.compareTo(aristaB));
	}	
}
