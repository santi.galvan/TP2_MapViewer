package negocio;

import java.util.ArrayList;

public class Negocio 
{
	RedElectrica redElectrica;
	AGM arbolGeneradorMinimo;
	Costos valor;
	
	public Negocio()
	{
		redElectrica = new RedElectrica();
		arbolGeneradorMinimo = new AGM();
		valor = new Costos();
	}
	//Entrada
	public void cargarLocalidad(String nombre, String provincia, int habitantes, double latitud, double longitud) 
	{
		redElectrica.agregarLocalidad(new Localidad(nombre, provincia, habitantes, latitud, longitud));
	}
	
	public void cargarCostos(double valorPorKm, double impuestoProvincial, double excesoDeKm)
	{
		valor.setValorPorKm(valorPorKm);
		valor.setImpuestoProvincial(impuestoProvincial);
		valor.setExcesoDeKm(excesoDeKm);
	}
	
	public void constuirRedOptima()
	{
		ArrayList<Localidad> V = redElectrica.localidades();
		ArrayList<Conexion> E = generarConexiones();
		ArrayList<Conexion> solucion = arbolGeneradorMinimo.kruskal(V, E); 
		
		cargarRed(solucion);
	}
		
	public boolean existeLocalidad(String nombre, String provincia, int habitantes, double lat, double lon)
	{
		return redElectrica.perteneceLocalidad(new Localidad(nombre, provincia, habitantes, lat, lon));
	}
	
	//Salida
	public ArrayList<Conexion> solucion()
	{
		return redElectrica.conexiones();
	}
	
	public double costoTotal()
	{
		double ret = 0;
		
		for(Conexion e : redElectrica.conexiones())
		{
			ret+=e.getPeso().floatValue();
		}
		return ret;
	}
	
	public void setRedElectrica(RedElectrica red)
	{
		this.redElectrica = red;
	}
	
	//De instancia
	ArrayList<Conexion> generarConexiones()
	{
		return Conexion.conexionesPosibles(redElectrica.localidades(), valor);
	}
	
	private void cargarRed(ArrayList<Conexion> aristasOptimas)
	{
		for(Conexion e : aristasOptimas)
			redElectrica.agregarConexion(e);
	}
	
	public Object getRedElectrica() 
	{
		return redElectrica;
	}
	
	public double[] getCostos(){
		double [] ret = {valor.getValorPorKm().floatValue(),valor.getImpuestoProvincial().floatValue(),valor.getExcesoDeKm().floatValue()};
		return ret;
	}
}
