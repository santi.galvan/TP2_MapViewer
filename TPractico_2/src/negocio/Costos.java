package negocio;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Costos
{
	private BigDecimal valorPorKm;
	private BigDecimal impuestoProvincial;
	private BigDecimal excesoDeKm;
	
	public Costos() 
	{
		this.valorPorKm = new BigDecimal(1);
		this.impuestoProvincial = new BigDecimal(0);
		this.excesoDeKm = new BigDecimal(0) ; 
	}
	
	public Costos(int valorPorKm, int impuestoProvincial, int excesoDeKm) 
	{
		this.valorPorKm = new BigDecimal(valorPorKm);
		this.impuestoProvincial = new BigDecimal(impuestoProvincial);
		this.excesoDeKm = new BigDecimal(excesoDeKm); 
	}
	
	//Costo total dado por los impuestos cargados por el usuario
	//Distancia en kilometros entre dos localidades
	public BigDecimal calcularCosto(Localidad origen, Localidad destino)
	{
		verificarEntrada(origen, destino);
		
		BigDecimal km = new BigDecimal(calcularKm(origen, destino));	
		km = km.setScale(4, RoundingMode.CEILING);
		
		BigDecimal costoTotal = km.multiply(valorPorKm);
		costoTotal = costoTotal.setScale(4, RoundingMode.CEILING);
		
		if(!origen.getProvincia().equals(destino.getProvincia()))
			costoTotal = costoTotal.add(impuestoProvincial);
		
		if(km.compareTo(new BigDecimal(200)) > 0)
			costoTotal = costoTotal.add(excesoDeKm);

		
		return costoTotal; //El casteo final es por error de redondeo, pinchada el loco
	}
	private void verificarEntrada(Localidad origen, Localidad destino) {
		if(origen == null || destino == null) throw new IllegalArgumentException(); 
			
			
	}

	//Distancia en kilometros entre dos localidades
	double calcularKm(Localidad origen, Localidad destino) 
	{
		double lat1 = origen.getLatitud();
		double lng1 = origen.getLongitud();
		double lat2 = destino.getLatitud();
		double lng2 = destino.getLongitud();
		
		double radioTierra = 6371; 
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return distancia;        
	}
	
	public BigDecimal getValorPorKm() 
	{
		return valorPorKm;
	}

	public BigDecimal getImpuestoProvincial() 
	{
		return impuestoProvincial;
	}

	public BigDecimal getExcesoDeKm() 
	{
		return excesoDeKm;
	}

	public void setValorPorKm(double valor) 
	{
		valorPorKm = new BigDecimal(valor);
	}

	public void setImpuestoProvincial(double valor) 
	{
		impuestoProvincial = new BigDecimal(valor);
	}

	public void setExcesoDeKm(double valor)
	{
		excesoDeKm = new BigDecimal(valor);
	}
}
