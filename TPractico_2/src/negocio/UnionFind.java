package negocio;

import java.util.Collection;
import java.util.HashMap;

public class UnionFind 
{
	HashMap<String,String> conjuntoDisjunto;
	//Estructura de Datos Union-Find adaptada para utilizarla con el algoritmo de Kruskal
	////Para esta implementacion, el m�todo Find devolver� una String ya que solo de usara internamente
	//No deber�a ser as�, ya que los m�todos union y find tendr�an que ser p�blicos
	//La relaci�n del HashMap es Elemento -> Otro elemento del conjunto
	//La cadena sigue hasta que Elemento - > Numero negativo que representa la ra�z y la altura del Arbol.
	//Recibo un conjunto con elementos y etiqueto a cada elemento con un String
	
	
	
	public UnionFind(Collection<?> objetos)
	{	
		conjuntoDisjunto = new HashMap<String,String>();
		
		for(Object o: objetos)
			conjuntoDisjunto.put(o.toString(), "-1");
	}

	//Devuelvo el representante del conjunto al cual pertenece el elemento
	String find(String elemento) 
	{	
		existeElemento(elemento);
		String contenidoDeElemento = conjuntoDisjunto.get(elemento);
		if(esRaiz(contenidoDeElemento))
			return elemento;
		
		conjuntoDisjunto.put(elemento, find(contenidoDeElemento));
		
		return conjuntoDisjunto.get(elemento);
	}
	
	//Uno el conjunto que tiene el elemento U, con el conjunto que tiene el elemento V
	public boolean union(Object u, Object v)
	{
		existeElemento(u.toString());
		existeElemento(v.toString());
		
		String raizU = this.find(u.toString());
		String raizV = this.find(v.toString());
			
		if(raizU.compareTo(raizV)==0)
			return false;
		
		unionAuxiliar(raizU,raizV);
		return true;
	}
	
	private void existeElemento(String elemento) 
	{
		if(!(conjuntoDisjunto.containsKey(elemento)))
			throw new IllegalArgumentException();
	}

	//Recibo los representantes del conjunto que tiene el elemento U, idem V.
	void unionAuxiliar(String representanteU, String representanteV)
	{	
		String raizU = conjuntoDisjunto.get(representanteU);
		String raizV = conjuntoDisjunto.get(representanteV);
		
		if(raizV.compareTo(raizU) < 0)
			conjuntoDisjunto.put(representanteU, representanteV);
		else
		{
			if(raizU.compareTo(raizV)==0)
			{
				Integer alturaDelArbol = Integer.valueOf((raizU));
				alturaDelArbol = alturaDelArbol -1;
				conjuntoDisjunto.put(representanteU, alturaDelArbol.toString());
			}	
			conjuntoDisjunto.put(representanteV, representanteU);
		}
	}
	
	//Intenta parsear algun elemento, 
	//y en caso de que sea distinto 
	//de un numero negativo, no es raiz 
	private boolean esRaiz(Object elemento)
	{
		try
		{
			Integer.valueOf(elemento.toString());
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	
	
}
