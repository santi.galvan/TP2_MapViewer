package negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class RedElectrica implements Serializable
{
	// Representamos el grafo por listas de vecinos
	private HashMap<Localidad,HashSet<Conexion>> conexiones;
	
	private static final long serialVersionUID = 1L;
	
	public RedElectrica()
	{
		conexiones = new HashMap<Localidad,HashSet<Conexion>>();
	}
	
	public RedElectrica(ArrayList<Localidad> localidades)
	{
		conexiones = new HashMap<Localidad,HashSet<Conexion>>();
		
		for(int i=0; i<localidades.size(); ++i)
			conexiones.put(localidades.get(i),new HashSet<Conexion>());
	}
	
	public void agregarLocalidad(Localidad localidad) 
	{
		conexiones.put(localidad, new HashSet<Conexion>());
	}
	
	public void agregarConexion(Conexion e)
	{
		conexiones.get(e.getOrigen()).add(e);
		conexiones.get(e.getDestino()).add(e);
	}
	
	public ArrayList<Localidad> localidades()
	{
		ArrayList<Localidad> ret = new ArrayList<>(); 
		
		for(Localidad vertice: conexiones.keySet())
		{
			ret.add(vertice);
		}
			
		return ret;
	}
	
	public ArrayList<Conexion> conexiones()
	{
		ArrayList<Conexion> ret = new ArrayList<>();
		HashSet<Conexion> auxiliar = new HashSet<>();
		
		for(HashSet<Conexion> conexiones : conexiones.values()) 
		{
			for(Conexion e : conexiones) {
				if(auxiliar.add(e)) 
				{
					ret.add(e);
				}
			}
		}
		return ret;
	}
	
	public boolean perteneceLocalidad(Localidad ciudad)
	{
		return conexiones.containsKey(ciudad);
	}
	
	//Getters
	public int cantLocalidades()
	{
		return conexiones.size();
	}
	
	public int cantConexiones()
	{
		return conexiones().size();
	}
	
	@Override
	public String toString() 
	{
		String ret = "";
		for(Localidad origen: conexiones.keySet()) 
			ret+= origen + " -> " + conexiones.get(origen) +"\n";
		
		return ret;
	}
	
}