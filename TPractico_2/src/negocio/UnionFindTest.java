package negocio;

import static org.junit.Assert.*;
import java.util.ArrayList;

import org.junit.Test;

public class UnionFindTest {

	@Test(expected = IllegalArgumentException.class)
	public void buscarElementoInexistenteTest()
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] {
				"San Miguel, Buenos Aires, 6764, -34.541913994732255, -58.717288970947266", 
				"Bella Vista, Buenos Aires, 23553, -34.56863420401555, -58.68913650512695"});
		
		UnionFind instancia = new UnionFind(localidades);
	
		instancia.find(new Localidad("Pilar City","Buenos Aires",70650,60,-9).toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void unirElementoInexistenteTest()
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] {
				"San Miguel, Buenos Aires, 6764, -34.541913994732255, -58.717288970947266", 
				"Bella Vista, Buenos Aires, 23553, -34.56863420401555, -58.68913650512695"});
		
		UnionFind instancia = new UnionFind(localidades);
	
		String idSanMiguel = localidades.get(0).toString();
		instancia.union(idSanMiguel, new Localidad("Pilar City","Buenos Aires",70650,60,-9));
	}
	
	@Test
	public void localidadesTest()
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] {
				"San Miguel, Buenos Aires, 6764, -34.541913994732255, -58.717288970947266", 
				"Bella Vista, Buenos Aires, 23553, -34.56863420401555, -58.68913650512695", 
				"Munro, Buenos Aires, 2343, -34.5292577800633, -58.52210998535156", 
				"Fonavi, Buenos Aires, 346, -34.44560067000515, -58.73711585998535",
				"Grand Bourg, Buenos Aires, 343, -34.488819248855194, -58.726537227630615",
				"Tortuguitas, Buenos Aires, 236, -34.47035281133538, -58.7584662437439"});
		
		UnionFind instancia = new UnionFind(localidades);
		
		instancia.union(localidades.get(0), localidades.get(1));
		instancia.union(localidades.get(0), localidades.get(2));
		instancia.union(localidades.get(4), localidades.get(3));
		
		String idSanMiguel = localidades.get(0).toString();
		String idBellaVista = localidades.get(1).toString();
		String idMunro = localidades.get(2).toString();
		String idFonavi = localidades.get(3).toString();
		String idGrandBourg = localidades.get(4).toString();
		String idTortuguitas = localidades.get(5).toString();
		
		assertEquals(idMunro, instancia.find(idSanMiguel));
		assertEquals(idMunro, instancia.find(idBellaVista));
		assertEquals(idMunro, instancia.find(idMunro));
		assertEquals(idGrandBourg, instancia.find(idGrandBourg));
		assertEquals(idGrandBourg, instancia.find(idFonavi));
		assertEquals(idTortuguitas, instancia.find(idTortuguitas));
	}
	
	@Test 
	public void aristasTest()
	{
		
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Fonavi, Buenos Aires, 346, -34.44560067000515, -58.73711585998535", 
				"Tortuguitas, Buenos Aires, 236, -34.47035281133538, -58.7584662437439", 
				"Grand Bourg, Buenos Aires, 343, -34.488819248855194, -58.726537227630615" });
		
		ArrayList<Conexion> conexiones = AuxiliaresTest.crearAristas(localidades, new String[] {
				"San Miguel  -> Fonavi",
				"Fonavi      -> Grand Bourg",
				"Tortuguitas -> Fonavi",
				"Grand Bourg ->	Tortuguitas" }); 
		
		UnionFind instancia = new UnionFind(conexiones);
		
		String idSanMiguelFonavi = conexiones.get(0).toString();
		String idFonaviGrandBourg = conexiones.get(1).toString();
		String idTortuguitasFonavi = conexiones.get(2).toString();
		String idGrandBourg_Tortuguitas = conexiones.get(3).toString();
		
		assertTrue(instancia.union(idSanMiguelFonavi, idFonaviGrandBourg));
		assertTrue(instancia.union(idGrandBourg_Tortuguitas, idFonaviGrandBourg));
		assertTrue(instancia.union(idGrandBourg_Tortuguitas, idTortuguitasFonavi));
	}

}
